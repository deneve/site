<?php

require_once __DIR__ . '/vendor/deneve/framework/deneve.php';
/*
 * Запуск фреймворка
 * Параметры конструктора:
 * @param string $appPath (путь к приложению)
 * @param string $basePath (базовая директория приложения. default: /)
 * @param int $debugMode (режим отладки; 0 = без ошибок, 1 = отладка, 2 = отладка+логирование, 3 = только логирование. default: 1)
 * @param string $configName (имя конфигурационного файла. default: main)
 */
(new frameworkDeneve(__DIR__ . '/protected'))->run();