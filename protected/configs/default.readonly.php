<?php
/*
 * Конфигурационный файл по умолчанию, не рекомендуется изменять.
 * Получить конфиг default: $array = $this->app->config->getDefault();
 * Получить актуальный, например, db['username']: $username = $this->config('components.db.username');
 */
return [
    'basePath' => null,
    'charset' => 'utf8',
    'language' => 'en',
    'languageToUrl' => false,

    'site' => [
        'name' => 'siteName'
    ],

    'default' => [
        'static' => 'static',
        'module' => 'site/default/index',
        'error404' => 'site/default/error404'
    ],

    'components' => [
        'auth' => [
            'hash' => ['fields' => 'password', 'method' => 'md5', 'solt' => null],
            'token' => ['enabled' => false, 'life' => 0]
        ],
        'db' => [
            'type' => 'mysql',
            'dsn' => 'mysql:host=localhost;dbname=databaseName',
            'username' => 'root',
            'password' => 'root',
            'charset' => 'utf8'
        ]
    ]
];