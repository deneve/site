<?php
/*
 * Конфигурационный файл,
 * дополняет или переписывает конфиг по умолчанию.
 * Получить конфиг default: $array = $this->app->config->getDefault();
 * Получить актуальный, например, db['username']: $username = $this->config('components.db.username');
 */
return [
    'basePath' => __DIR__ . DIRECTORY_SEPARATOR . '..',
    'language' => 'ru',
    'languageToUrl' => false,

    'site' => [
        'name' => 'DENEVE PHP Framework',
        'desc' => 'MVC. Simple. Fast. Open.',
        'descFull' => '',
        'author' => 'Dmitry Sergeev'
    ],

    'default' => [
        'module' => 'site/pages/index',
        'error404' => 'site/pages/error404'
    ],

    'components' => [
        'db' => [
            'dsn' => 'mysql:host=localhost;dbname=deneve_site',
            'username' => 'root',
            'password' => 'root'
        ]
    ]
];
