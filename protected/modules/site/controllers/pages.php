<?php

namespace site;

class pagesController extends \Core\Controller
{
    /**
     * страница 404 Not Found
     * @return object layout "current|default", view "404"
     */
    public function error404Action()
    {
        $this->titlePage = "Страница не существует";

        return $this->app->render->view('404');
    }

    /**
     * страница Главная
     * @return object layout "current|default", view "index"
     */
    public function indexAction()
    {
        $this->titlePage = "Главная";

        return $this->app->render([
            'download' => $this->app->partial('index/download'),
            'firststeps' => $this->app->partial('index/firststeps'),
            'contacts' => $this->app->partial('index/contacts')
        ])->view('index');
    }

    /**
     * ajax Поиск по API
     * @return string
     */
    public function searchAction()
    {
        return 'Поиск по API...';
    }

    /**
     * страница API
     * @return object layout "current|default", view "api"
     */
    public function apiAction($choice = null)
    {
        $this->titlePage = "API";

        $modelApi = $this->app->model('api');

        return $this->app->render->view('api', [
            'modelApi' => $modelApi->getApis()
        ]);
    }

    /**
     * страница О фреймворке
     * @return object layout "current|default", view "about"
     */
    public function aboutframeworkAction()
    {
        $this->titlePage = "О Фреймворке";

        return $this->app->render->view('about', [
            'info' => null
        ]);
    }

    /**
     * страница Изменения
     * @return object layout "current|default", view "changes"
     */
    public function changesAction()
    {
        $this->titlePage = "Изменения";

        return $this->app->render->view('changes');
    }
}