<!-- FIRSTSTEPS start -->
<section id="firststeps" style="background-color: rgba(40, 40, 40, 0.5);">
    <div class="container">
        <div class="row no-gutter">
            <div class="col-lg-12 text-center">
                <h2 class="margin-top-0 text-primary">Первые шаги</h2>
                <hr class="primary">
                <h5 style="text-align: center;">
                    Вкратце — что нужно знать и куда двигаться дальше, для более детального изучения читайте, пожалуйста,
                    <?= $app->t->a('Документацию', ['href' => $app->u('docs')]) ?> и <?= $app->t->a('API', ['href' => $app->u('api')]) ?>.
                </h5>
                <div class="text-left margin-top-40">
                    <div class="col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <span class="text-info">1.</span> Структура фреймворка
                            </div>
                            <small class="panel-body code">
                                После установки или распаковки архива, у Вас должен быть каталог примерно следующей структуры:
                                <a title="Структура фреймворка #1" href="<?= $app->s('images/screens/1.structure_framework_1.png') ?>" class="fancybox" rel="structure_framework"><span class="glyphicon glyphicon-open"></span></a><br/>
                                <hr/>
                                "index.php" - главный и входящий скрипт приложения
                                "protected" - каталог сайта, сдержащий слои, модули и другие разделы<br/>
                                "static" - статические файлы и другие ресурсы (js, css, images, fonts, etc)<br/>
                                "vendor" - фреймворк и сторонние библиотеки
                                <hr/>
                                "vendor/deneve_framework":
                                <a title="Структура фреймворка #2" href="<?= $app->s('images/screens/1.structure_framework_2.png') ?>" class="fancybox" rel="structure_framework"><span class="glyphicon glyphicon-open"></span></a><br/>
                                "deneve.php" - главный скрипт фреймворка<br/>
                                "core" - каталог с скриптами ядра<br/>
                                "libs" - встроенные библиотеки, такие как [db, request, session, filter, render, partial, cache и др]
                            </small>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <span class="text-info">2.</span> Структура сайта
                            </div>
                            <small class="panel-body code">
                                По умолчанию сайт находится в каталоге "protected".<br/>
                                Основные рабочие каталоги здесь - "configs" (конфиги), "layouts" (слои), "modules" (модули).<br/><br/>
                                Однако большее время разработки отводится на "modules",
                                структура которого строится на MVC (модель, вид, контроллер) архитектуре, разбитого на модули
                                (части; например: site, admin, cabinet). <a href=""><span class="glyphicon glyphicon-open"></span></a>
                                <hr/>
                                Немного скриншотов:<br/>
                                &emsp;1. Это небольшая модель: <a href=""><span class="glyphicon glyphicon-open"></span></a><br/>
                                &emsp;2. А это слои, виды, части: <a href=""><span class="glyphicon glyphicon-open"></span></a> <a href=""><span class="glyphicon glyphicon-open"></span></a> <a href=""><span class="glyphicon glyphicon-open"></span></a><br/>
                                &emsp;3. Ну и, конечно, контроллер: <a href=""><span class="glyphicon glyphicon-open"></span></a>
                                <hr/>
                                Также рекомендуем следующие разделы:<br/>
                                &emsp;<a href="<?= $app->u('examples') ?>">Смотреть примеры</a>, <a href="<?= $app->u('docs') ?>">документацию</a>
                            </small>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <span class="text-info">3.</span> Перед началом работы
                            </div>
                            <small class="panel-body code">
                                В любой части слоев "layouts", видов "views" и "partials" доступен объект $app,
                                а в контроллерах (controllers), моделях (models), библиотеках (libs) через this ($this->app).<br/><br/>
                                <span class="text-primary"><b>App</b></span> - это главный объект фреймворка, и он очень легкий и изначально не загружает никаких библиотек или других компонентов
                                (за исключением тонкого ядра). Тем не менее, через него можно сразу обращаться к любым библиотекам
                                (request, db, layout, filter, session и другие) без предварительной инициализации. <span class="text-danger">*</span><br/><br/>
                                <div style="font-size: 12px; line-height: 1;">
                                    <span class="text-danger">*</span> Благодаря магическим методам __call и __get, при получении имени библиотеки,
                                    фреймворк загружает ее в виде синглетона, а далее позволяет просто обращаться к любым методам этой библиотеки.
                                    Такой подход позволяет не беспокоиться о предварительных загрузках компонентов, фактически всё доступно сразу,
                                    но загрузка происходит только при первом востребовании. Это превращает запросы в сверхлегкие и быстрые,
                                    с минимальным количеством зависимостей.
                                </div>
                            </small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- FIRSTSTEPS end -->
<script>
    $(function () {
        $(".fancybox").fancybox({
            padding: 5
        });
    });
</script>