<!-- CONTACTS start -->
<aside id="contacts" class="bg-dark">
    <div class="container text-center">
        <h2 class="text-primary">Контакты</h2>
        <hr/>
        <br>
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1">
                <div class="row">
                    <form class="row">
                        <div class="col-md-4">
                            <input type="text" class="form-control" placeholder="Имя">
                        </div>
                        <div class="col-md-4">
                            <input type="text" class="form-control" placeholder="Email">
                        </div>
                        <div class="col-md-4">
                            <input type="text" class="form-control" placeholder="Тип">
                        </div>
                        <div class="col-md-12">
                            <br/>
                            <textarea class="form-control" rows="6" placeholder="Your message here..">
                                types: Пожелания, Сообщить о баге на сайте, Сообщить о баге в фреймворке, Другая тема
                            </textarea>
                        </div>
                        <div class="col-md-4 col-md-offset-4">
                            <br/>
                            <button type="button" data-toggle="modal" data-target="#alertModal" class="btn btn-block btn-lg btn-transparent">Отправить</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</aside>
<!-- CONTACTS end -->