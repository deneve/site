<!-- DOWNLOAD start -->
<section id="download">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="margin-top-0 text-primary">Скачать <?= $app->config('site.name') ?></h2>
                <hr class="primary">
                <div class="col-md-12 text-muted" style="margin: 20px 0 40px 0;">
                    Рекомендуется <span class="text-primary">PHP 5.6+</span>, <span class="text-primary">MySQL PDO</span><br/>
                    Версия фреймворка: <span class="text-success"><?= $app->get('version') ?></span> <a href="<?= $app->u('changes') ?>"><small>[изменения]</small></a>, статус: <span class="text-info">alpha</span><br/>
                    <small>
                        Git (фреймворк): <a href="https://mico127@bitbucket.org/deneve/framework.git">https://mico127@bitbucket.org/deneve/framework.git</a><br/>
                        Git (сайт, тесты): <a href="https://mico127@bitbucket.org/deneve/site.git">https://mico127@bitbucket.org/deneve/site.git</a>
                    </small>
                </div>
                <div class="col-md-6 text-left">
                    <h3>Установка Composer-ом</h3>
                    <small class="code" style="border-radius: 6px;">
                        ▌ <s>php composer.phar create-project denevedist/deneve-clear start lastv</s><br/>
                        <hr/>
                        ▌ Читаем раздел "Первые шаги". Готово!<br/>
                    </small>
                </div>
                <div class="col-md-6 text-left">
                    <h3>Прямая ссылка</h3>
                    <small class="code" style="border-radius: 6px;">
                        ▌ <a href="#">http://deneve/download/deneve.version.zip</a><br/>
                        <hr/>
                        ▌ Читаем раздел "Первые шаги". Готово!<br/>
                    </small>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- DOWNLOAD end -->