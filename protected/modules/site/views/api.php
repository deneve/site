<header>
    <table width="100%" style="margin: 70px 0 20px 0;">
        <tr>
            <td valign="top" width="255px">
                <div style="position: fixed; left: 16px; width: 220px;">
                <?php $app->helper->addActiveBegin('text-primary', ['checkParams' => true]) ?>
                <?php foreach ($modelApi as $api) { ?>
                    <div class="panel text-left">
                        <a href="" class="js-api"><div class="panel-heading"><?= $api->title ?></div></a>
                        <ul class="list-group" style="font-size: 14px; display: none;">
                        <?php foreach ($api->subs as $sub) { ?>
                            <a href="<?= $app->u('site/pages/api/' . $sub->name) ?>"><li class="list-group-item"><span class="glyphicon glyphicon-link"></span> <?= $sub->title ?></li></a>
                        <?php } ?>
                        </ul>
                    </div>
                <?php } ?>
                <?php $app->helper->addActiveEnd() ?>
                </div>
            </td>
            <td valign="top" class="text-left" style="padding: 12px 18px; background-color: #ebebeb; color: #0a0a0a; border-radius: 3px 0 0 3px;">

            </td>
        </tr>
    </table>
</header>

<script>
$(function() {
    $('header .js-api').on('click', function() {
        var subs = $(this).closest('div').find('.list-group');
        if (subs.css('display') == 'none') {
            subs.show();
        } else {
            subs.hide();
        }
        return false;
    });
    $('header table tr td:first ul a.text-primary').closest('div').find('.js-api').click();
});
</script>

