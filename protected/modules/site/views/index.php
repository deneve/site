<a id="topNavBitbucket" href="https://bitbucket.org/deneve" target="_blank">
    <img src="<?= $app->s('images/pic/bitbucket_logo.png') ?>">
</a>

<!-- HOME start -->
<header id="home">
    <div class="header-content">
        <div class="inner">
            <h1 class="cursive">
                <?= $app->config('site.name') ?>
            </h1>
            <h3 class="margin-top-20" style="color: #d8e5ed; text-shadow:1px 0px 1px #000;">
                <?= $app->config('site.desc') ?>
            </h3>
        </div>
        <div class="container margin-top-40">
            <div class="row">
                <div class="col-lg-4 col-md-4 text-center">
                    <div class="feature">
                        <h3>M. - models</h3>
                        <p style="color: #96de49; text-shadow: 0 1px 1px rgba(60, 60, 60, 0.5);">$this->app->db и толстый код модели</p>
                        <small class="code text-left" style="border-radius: 6px;">
                            $modelUsers = $this->app->model('users');<br/>
                            $allUsers = $modelUsers->find(); // все<br/>
                            // Найдем Елизавету, которой 20 лет<br/>
                            if ($personLiza = $modelUsers->findOne(<br/>
                            &emsp;'name = ? and age = ?', ['Лиза', 20]<br/>
                            )) {<br/>
                            &emsp;$personLiza->age = 18; // теперь ей 18 лет<br/>
                            &emsp;$personLiza->save(); // сохраним изменения<br/>
                            &emsp;$personLiza->delete(); // удалим запись из таблицы<br/>
                            }
                        </small>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 text-center">
                    <div class="feature">
                        <h3>V. - views</h3>
                        <p style="color: #96de49; text-shadow: 0 1px 1px rgba(60, 60, 60, 0.5);">$app и Html, шаблоны, другое</p>
                        <small class="code text-left" style="border-radius: 6px;">
                            &lt;header&gt;&lt;?= $app->partial('header') ?&gt;&lt;/header&gt;<br/>
                            &lt;div class='content'&gt;<br/>
                            &emsp;&lt;h1&gt;Hello World!&lt;/h1&gt;<br/>
                            &emsp;&lt;p&gt;Happier than you and me&lt;/p&gt;<br/>
                            &emsp;...<br/>
                            &emsp;&lt;?= $app->t->a('home', ['href' => '/']) ?&gt;<br/>
                            &lt;/div&gt;<br/>
                            &lt;footer&gt;<br/>
                            &emsp;&lt;?= $app->partial('otherModule', 'footer') ?&gt;<br/>
                            &lt;/footer&gt;
                        </small>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 text-center">
                    <div class="feature">
                        <h3>C. - controllers</h3>
                        <p style="color: #96de49; text-shadow: 0 1px 1px rgba(60, 60, 60, 0.5);">$this->app и тонкий контроллер</p>
                        <small class="code text-left" style="border-radius: 6px;">
                            public function indexAction($id)<br/>
                            {<br/>
                            &emsp;if ($this->app->request->get('info')) {<br/>
                            &emsp;&emsp;$usr = $this->app->db('usrs')->findOne($id);<br/>
                            &emsp;&emsp;$response = $this->app->config('site.name');<br/>
                            &emsp;}<br/>
                            &emsp;return $this->app->render->view('index', [<br/>
                            &emsp;&emsp;'resp' => isset($response) ? $response : null<br/>
                            &emsp;]);<br/>
                            }
                        </small>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <video id="video-background" autoplay="" loop="" class="fillWidth fadeIn wow collapse in" style="filter:hue-rotate(20deg) blur(1.5px); -webkit-filter:hue-rotate(20deg) blur(1.5px); -ms-filter:hue-rotate(20deg) blur(1.5px);"
           poster="<?= $app->s('images/des/bg.png') ?>">
        <source src="" type="video/mp4">Your browser does not support the video tag. I suggest you upgrade your browser.
    </video>
</header>
<!-- HOME end -->