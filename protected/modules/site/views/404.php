<!-- ERROR 404 start -->
<header id="home">
    <div class="header-content">
        <div class="inner">
            <h1 class="cursive">
                <?= $app->config('site.name') ?>
            </h1>
            <h3 class="margin-top-20" style="color: #d8e5ed; text-shadow:1px 0px 1px #000;">
                <?= $app->config('site.desc') ?>
            </h3>
        </div>
        <div class="container margin-top-40">
            <div class="row">
                <div class="col-lg-12 col-md-12 text-center">
                    <h3>Страница не найдена :(</h3>
                    <a href="<?= $app->u() ?>">Главная</a>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- ERROR 404 end -->