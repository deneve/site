<?php

namespace admin;

class pagesController extends \Core\Controller
{
    public $titlePage = 'Админ / ';

    /**
     * action Главная страница админ-панели
     */
    public function indexAction()
    {
        $this->titlePage.= 'Главная';

        return $this->app->render->view('index');
    }

    /**
     * action Страница авторизации
     */
    public function loginAction()
    {
        $this->titlePage.= 'Авторизация';

        if ($this->app->auth->is()) {
            $this->app->redirect('index');
        }

        $model = $this->app->model('users');

        if ($this->app->request->isPost()) {
            $model->setAttributes = $this->app->request->post();
            if ($model->validate()) {
                if ($this->app->auth->login($model, 'login', 'password')) {
                    $this->app->redirect('index');
                }
            }
        }

        return $this->app->render->view('login', ['model' => $model]);
    }

    /**
     * action Страница API
     */
    public function apiAction($param)
    {
        $this->titlePage = 'API';

        $modelApi = $this->app->model('api');

        if ($this->app->request->isAjax()) {
            if ($this->app->request->post('save_api')) {
                $this->app->db($modelApi->getTableName())->update(
                    $this->app->request->post('id'),
                    'sort = ?, title = ?',
                    [$this->app->request->post('data')['sort'], $this->app->request->post('data')['title']]
                );
            }
            exit;
        }

        return $this->app->render->view('api', [
            'modelApi' => $modelApi->getApis(),
            'apiTexts' => !empty($param) ? $this->app->model('apiSubs')->findOne('name = ?', $param)->texts() : []
        ]);
    }

    /**
     * action Выход
     */
    public function logoutAction()
    {
        if ($this->app->auth->is() && $this->app->auth->logout()) {
            $this->app->redirect('login');
        }
    }
}