<?php

class adminModule extends \Core\Module
{
    /**
     * Инициализация модуля
     * Если не авторизован, не пускаем никуда, кроме страницы авторизации
     */
    public function inic()
    {
        if ($this->app->get('action') != 'login' && !$this->app->auth->is()) {
            $this->app->redirect('login');
        }
    }
}