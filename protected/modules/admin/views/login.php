<!-- HOME start -->
<header id="home">
    <div class="header-content">
        <div class="inner">
            <h1 class="cursive">
                <?= $app->config('site.name') ?>
            </h1>
            <h3 class="margin-top-20" style="color: #d8e5ed; text-shadow:1px 0px 1px #000;">
                <?= _('Админ-панель') ?>
            </h3>
        </div>
        <div class="container margin-top-40">
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4 text-center">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <?php $app->helper->formBegin($model, ['name' => 'formLogin', 'method' => 'post']) ?>
                                <div class="form-group">
                                    <label for="loginLabel"><?= _('Логин') ?></label>
                                    <input id="loginLabel" name="login" type="text" placeholder="Login" maxlength="55" class="form-control">
                                    <small id="loginError" class="text-danger"></small>
                                </div>
                                <div class="form-group">
                                    <label for="passwordLabel"><?= _('Пароль') ?></label>
                                    <input id="passwordLabel" name="password" type="password" placeholder="Password" maxlength="55" class="form-control">
                                    <small id="passwordError" class="text-danger"></small>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox"> <?= _('Запомнить') ?>
                                    </label>
                                </div>
                                <button type="submit" class="btn btn-default"><?= _('Войти') ?></button>
                            <?php $app->helper->formEnd() ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-4"></div>
            </div>
        </div>
    </div>
    <video id="video-background" autoplay="" loop="" class="fillWidth fadeIn wow collapse in" style="filter:hue-rotate(20deg) blur(1.5px); -webkit-filter:hue-rotate(20deg) blur(1.5px); -ms-filter:hue-rotate(20deg) blur(1.5px);"
           poster="<?= $app->s('images/des/bg.png') ?>">
        <source src="" type="video/mp4">Your browser does not support the video tag. I suggest you upgrade your browser.
    </video>
</header>
<!-- HOME end -->