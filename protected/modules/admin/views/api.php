<div class="modal fade js-api-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Modal title</h4>
            </div>
            <div class="modal-body">
                <p>One fine body&hellip;</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>

<header>
    <table width="100%" style="margin: 70px 0 20px 0;">
        <tr>
            <td valign="top" width="255px">
                <div style="position: fixed; left: 16px; width: 220px;">
                    <div>
                        <button data-type="add" class="btn btn-sm btn-primary js-api-items">Добавить пункт</button>
                        <button data-type="remove" class="btn btn-sm btn-danger js-api-items">Удалить</button>
                    </div><br/>
                    <?php $app->helper->addActiveBegin('text-primary', ['checkParams' => true]) ?>
                    <?php foreach ($modelApi as $api) { ?>
                    <div class="panel text-left">
                        <div data-id="<?= $api->id ?>" class="panel-heading js-api-inputs">
                            <input type="text" value="<?= $api->sort ?>" size="1">&nbsp;
                            <input type="text" value="<?= $api->title ?>" size="12">
                        </div>
                        <ul class="list-group" style="font-size: 14px;">
                            <?php foreach ($api->subs as $sub) { ?>
                            <li class="list-group-item">
                                <input type="text" value="<?= $sub->sort ?>" size="1">&nbsp;
                                <a href="<?= $app->u('admin/pages/api/' . $sub->name) ?>"> <?= $sub->title ?></a>
                            </li>
                            <?php } ?>
                        </ul>
                    </div>
                    <?php } ?>
                    <?php $app->helper->addActiveEnd() ?>
                </div>
            </td>
            <td valign="top" class="text-left" style="padding: 12px 18px; background-color: #ebebeb; color: #0a0a0a; border-radius: 3px 0 0 3px;">
                <?php if (!count($apiTexts)) { ?>
                <h3 class="text-muted" style="padding: 100px 0;">
                    &emsp; <span class="glyphicon glyphicon-arrow-left"></span> &emsp; Выберите раздел
                </h3>
                <?php } else { ?>
                <?php foreach ($apiTexts as $text) { ?>
                <div>
                    <textarea rows="5" style="margin-bottom: 4px; width: 100%;"><?= $text->text ?></textarea>
                    <input type="text" value="" size="1">&nbsp;
                    <button class="btn btn-sm btn-info">Превью</button>
                    <button class="btn btn-sm btn-success">Сохранить</button>
                    <button class="btn btn-sm btn-danger">Удалить</button>
                    <hr/>
                </div>
                <?php } ?>
                <?php } ?>
            </td>
        </tr>
    </table>
</header>

<script>
$(function() {
    // click: кнопки добавить/удалить пункты
    $('header .js-api-items').on('click', function() {
        $('.js-api-modal').modal('show');
    });

    // blur: смена сортировки и названия пунктов
    $('header').on('blur', '.js-api-inputs', function() {
        var _this = $(this);
        $.ajax({data: {save_api: 1, id: _this.data('id'), data: {sort: _this.find('input:eq(0)').val(), title: _this.find('input:eq(1)').val()}}, dataType: 'json', method: 'post',
            beforeSend: function() {
                _this.find('input').prop({disabled: true});
            },
            complete: function() {
                _this.find('input').prop({disabled: false});
            }
        });
    });
});
</script>