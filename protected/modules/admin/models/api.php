<?php

namespace admin;

class apiModel extends \Core\Model
{
    /**
     * const's: REL_ONE - find one; REL_ALL - find all
     * 'name' => [one|all => 'rel_table'] // связь по id => id
     * 'name' => [one|all => 'rel_table(customId)'] // связь по id => customId
     * 'name' => [one|all => 'rel_table(customId1, customId2)'] // связь по customId1 => customId2
     */
    public function relations()
    {
        return [
            'subs' => [self::REL_ALL => $this->app->model('apiSubs')->getTableName() . '(parent_id)'],
        ];
    }

    public function getApis()
    {
        // cache add
        $apis = [];
        $modelApis = $this->find('', null, ['order' => 'sort']);
        foreach ($modelApis as $api) {
            $obj = new \stdClass;
            $obj->id = $api->id;
            $obj->title = $api->title;
            $obj->sort = $api->sort;
            $obj->subs = $api->subs();
            $apis[] = $obj;
        }
        return $apis;
    }
}
