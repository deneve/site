<?php

namespace admin;

class usersModel extends \Core\Model
{
    public function rules()
    {
        return [
            'default' => [
                'login' => 'min(2), max(55), errorText(* Введите логин.)',
                'password' => 'min(2), max(55), errorText(* Введите пароль.)',
            ]
        ];
    }
}
