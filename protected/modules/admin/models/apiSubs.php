<?php

namespace admin;

class apiSubsModel extends \Core\Model
{
    protected $_tableName = 'api_subs';

    public function relations()
    {
        return [
            'texts' => [self::REL_ALL => $this->app->model('apiTexts')->getTableName() . '(parent_id)'],
        ];
    }
}
