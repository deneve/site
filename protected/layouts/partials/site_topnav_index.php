<div id="aboutModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h2 class="text-center">---</h2>
                <h5 class="text-center">

                </h5>
                <p class="text-justify">
                    desc
                </p>
                <p class="text-center"><a href="">Download Framework</a></p>
                <br/>
                <button class="btn btn-primary btn-lg center-block" data-dismiss="modal" aria-hidden="true"> OK </button>
            </div>
        </div>
    </div>
</div>

<!-- TOPNAV start -->
<div id="search"></div>

<nav id="topNav" class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <a href="#home" class="navbar-brand page-scroll">
                <span class="glyphicon glyphicon-flash"></span> DENEVE <small>v.<?= $app->get('version') ?></small>
            </a>
        </div>
        <div class="navbar-collapse collapse">
            <span id="navSpy">
            <ul class="nav navbar-nav">
                <li>
                    <a href="#download" class="page-scroll">Скачать</a>
                </li>
                <li>
                    <a href="#firststeps" class="page-scroll">Первые шаги</a>
                </li>
            </ul></span>
            <? $app->helper->addActiveBegin() ?>
            <ul class="nav navbar-nav">
                <li>
                    <a href="<?= $app->u('docs') ?>">Документация</a>
                </li>
                <li>
                    <a href="<?= $app->u('api') ?>">API</a>
                </li>
                <li>
                    <a href="<?= $app->u('examples') ?>">Примеры</a>
                </li>
                <li>
                    <a title="О проекте" href="#aboutModal" data-toggle="modal">О проекте</a>
                </li>
            </ul>
            <? $app->helper->addActiveEnd() ?>
            <ul class="nav navbar-nav navbar-right text-right">
                <li>
                    <a href="javascript:main.show_search()">Поиск по API</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<!-- TOPNAV end -->