<div id="alertModal" tabindex="-1" role="dialog" aria-hidden="true" class="modal fade">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body">
                <h2 class="text-center">Отлично!</h2>
                <p class="text-center">Вы подписались на обновления!</p>
                <br/>
                <button data-dismiss="modal" aria-hidden="true" class="btn btn-transparent btn-lg center-block">OK</button>
            </div>
        </div>
    </div>
</div>

<!-- FOOTER 1 start -->
<footer id="footer1">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-6 col-sm-3 column">
                <h4>Информация</h4>
                <ul class="list-unstyled">
                    <li><a href="<?= $app->u('license') ?>">Условия использования</a></li>
                    <li><a href="<?= $app->u('docs') ?>">Документация</a> и <a href="<?= $app->u('api') ?>">API</a></li>
                    <li><a href="https://bitbucket.org/deneve" target="_blank">Git BitBucket</a></li>
                    <li><a href="<?= $app->u('examples') ?>">Примеры приложений</a></li>
                </ul>
            </div>
            <div class="col-xs-6 col-sm-3 column">
                <h4>О нас</h4>
                <ul class="list-unstyled">
                    <li><a href="<?= $app->u('aboutFramework') ?>">О фреймворке</a></li>
                    <li><a href="<?= $app->u('contacts') ?>">Контакты</a></li>
                    <li><a href="<?= $app->u('donate') ?>">Поддержать нас</a></li>
                </ul>
            </div>
            <div class="col-xs-12 col-sm-3 column">
                <h4>Подпишись!</h4>
                <form>
                    <div class="form-group">
                        <input type="text" title="No spam, we promise!" placeholder="Email..." class="form-control">
                    </div>
                    <div class="form-group">
                        <button data-toggle="modal" data-target="#alertModal" type="button" class="btn btn-primary btn-transparent">Подписаться на обновления</button>
                    </div>
                </form>
            </div>
            <div class="col-xs-12 col-sm-3 text-right">
                <h5 class="text-muted"><i>Комьюнити</i>&emsp;</h5>
                <ul class="list-inline">
                    <li><a rel="nofollow" href="" title="Git"><img src="<?= $app->s('images/pic/icons/social-git-32.png') ?>"></a></li>
                    <li><a rel="nofollow" href="" title="Vkontakte"><img src="<?= $app->s('images/pic/icons/social-vk-32.png') ?>"></a></li>
                    <li><a rel="nofollow" href="" title="Facebook"><img src="<?= $app->s('images/pic/icons/social-fb-32.png') ?>"></a>&nbsp;</li>
                    <li><a rel="nofollow" href="" title="Twitter"><img src="<?= $app->s('images/pic/icons/social-tw-32.png') ?>"></a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>
<!-- FOOTER 1 end -->