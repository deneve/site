<!-- FOOTER 2 start -->
<footer id="footer2">
    <div class="container-fluid">
        <span class="pull-left text-muted small">
            This site is powered by <a href="<?= $app->basePath() ?>"><?= $app->config('site.name') ?></a>. All Rights Reserved.
        </span>
        <span class="pull-right text-muted small">
            Copyright &copy; <?= date('Y') ?> &nbsp;/&nbsp; <?= $app->config('site.author') ?>
        </span>
    </div>
</footer>
<!-- FOOTER 2 end -->