<?= $app->t(false)->meta('', [
    ['charset' => $app->config('charset')],
    ['name' => 'viewport', 'content' => 'width=device-width, initial-scale=1']
]) ?>
<?= $app->t(false)->link('', [
    ['rel' => 'stylesheet', 'href' => '/vendor/components/bootstrap/css/bootstrap.min.css'],
    ['rel' => 'stylesheet', 'href' => $app->s('css/bootstrap-over.css')]
]) ?>
<?= $app->t->script('', ['src' => '/vendor/components/jquery/jquery.min.js']) ?>