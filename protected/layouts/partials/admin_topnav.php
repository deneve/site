<!-- TOPNAV start -->
<div id="search"></div>

<nav id="topNav" class="navbar navbar-default navbar-fixed-top" style="background-color: #222222;">
    <div class="container-fluid">
        <div class="navbar-header">
            <a href="<?= $app->u('index') ?>" class="navbar-brand">
                <span class="glyphicon glyphicon-flash"></span> DENEVE <small>v.<?= $app->get('version') ?></small>
            </a>
        </div>
        <div class="navbar-collapse">
            <?= $app->helper->addActiveBegin('active', ['parent' => true]) ?>
            <ul class="nav navbar-nav">
                <li>
                    <a href="<?= $app->u('users') ?>">Пользователи</a>
                </li>
                <li>
                    <a href="<?= $app->u('docs') ?>">Документация</a>
                </li>
                <li>
                    <a href="<?= $app->u('api') ?>">API</a>
                </li>
                <li>
                    <a href="<?= $app->u('examples') ?>">Примеры</a>
                </li>
            </ul>
            <?= $app->helper->addActiveEnd() ?>
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="<?= $app->u('logout') ?>">Выйти</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<!-- TOPNAV end -->