<!-- TOPNAV start -->
<div id="search"></div>

<nav id="topNav" class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <a href="<?= $app->u() ?>" class="navbar-brand page-scroll">
                <span class="glyphicon glyphicon-flash"></span> DENEVE <small>v.<?= $app->get('version') ?></small>
            </a>
        </div>
        <div class="navbar-collapse collapse">
            <? $app->helper->addActiveBegin('active', ['parent' => true]) ?>
            <ul class="nav navbar-nav">
                <li>
                    <a href="<?= $app->u('docs') ?>">Документация</a>
                </li>
                <li>
                    <a href="<?= $app->u('api') ?>">API</a>
                </li>
                <li>
                    <a href="<?= $app->u('examples') ?>">Примеры</a>
                </li>
            </ul>
            <? $app->helper->addActiveEnd() ?>
            <ul class="nav navbar-nav navbar-right text-right">
                <li>
                    <a href="javascript:main.show_search()">Поиск по API</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<!-- TOPNAV end -->