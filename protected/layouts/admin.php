<!DOCTYPE html>
<html lang="<?=$app->config('language')?>">
<head>
    <?= $app->partial->layout('default_head') ?>
    <?= $app->t(false)->link('', [
        ['rel' => 'stylesheet', 'href' => $app->s('css/site.css')],
        ['rel' => 'stylesheet', 'href' => $app->s('css/media.css')],
        ['type' => 'image/x-icon', 'rel' => 'shortcut icon', 'href' => $app->s('images/pic/icons/icon-16.png')],
    ]) ?>
    <?= _($app->t->title($title)) ?>
</head>
<body>
    <img id="ajax" src="<?= $app->s('images/pic/load1.svg') ?>">
    <div id="sys-messages"></div>

    <?= $app->auth->is() ? $app->partial->layout('admin_topnav') : '' ?>

    <?= $content ?>

    <?= $app->partial->layout('site_footer2') ?>

    <?= $app->t(false)->link('', [
        ['rel' => 'stylesheet', 'href' => $app->s('js/highlight/styles/obsidian.css')]
    ]) ?>

    <?= $app->t->script('', [
        ['src' => '/vendor/components/bootstrap/js/bootstrap.min.js'],
        ['src' => $app->s('js/highlight/highlight.pack.js')]
    ]) ?>
</body>
</html>