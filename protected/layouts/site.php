<!DOCTYPE html>
<html lang="<?=$app->config('language')?>">
<head>
    <?= $app->partial->layout('default_head') ?>
    <?= $app->t(false)->link('', [
        ['rel' => 'stylesheet', 'href' => $app->s('css/site.css')],
        ['rel' => 'stylesheet', 'href' => $app->s('css/media.css')],
        ['type' => 'image/x-icon', 'rel' => 'shortcut icon', 'href' => $app->s('images/pic/icons/icon-16.png')],
    ]) ?>
    <?= $app->t->script('', ['src' => $app->s('js/index.js')]) ?>
    <?= $app->t->title($title) ?>
</head>
<body>
    <img id="ajax" src="<?= $app->s('images/pic/load1.svg') ?>"/>
    <div id="sys-messages"></div>

    <?php $isIndex = (empty($app->get('action')) || $app->get('action') == 'index'); ?>

    <?= $isIndex ? $app->partial->layout('site_topnav_index') : $app->partial->layout('site_topnav_other') ?>

    <?= $content ?>

    <?php
    if ($isIndex) {
        echo $download . $firststeps . $contacts . $footer . $app->partial->layout('site_footer1');
    }
    ?>

    <?= $app->partial->layout('site_footer2') ?>

    <?= $app->t(false)->link('', [
        ['rel' => 'stylesheet', 'href' => '/vendor/components/fancybox/source/jquery.fancybox.css'],
        ['rel' => 'stylesheet', 'href' => $app->s('css/animate.min-3.1.1.css')],
        ['rel' => 'stylesheet', 'href' => $app->s('css/jquery.bxslider.css')],
        ['rel' => 'stylesheet', 'href' => $app->s('js/highlight/styles/obsidian.css')],
    ]) ?>

    <?= $app->t->script('', [
        ['src' => '/vendor/components/bootstrap/js/bootstrap.min.js'],
        ['src' => '/vendor/components/fancybox/source/jquery.fancybox.pack.js'],
        ['src' => $app->s('js/ajax-libs-wow-1.1.2.js')],
        ['src' => $app->s('js/jquery.autosize.min.js')],
        ['src' => $app->s('js/jquery.bxslider.min.js')],
        ['src' => $app->s('js/highlight/highlight.pack.js')],
        ['src' => '//cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js']
    ]) ?>
</body>
</html>