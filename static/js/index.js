'use strict';

$(document).ajaxStart(function() {$('#ajax').show();});
$(document).ajaxStop(function() {$('#ajax').hide();});

var load = $('<div>', {class: 'load'});

// для работы с локальным хранилищем
var lstorage = {
    o: function() {
        return window.localStorage ? window.localStorage : false;
    },
    s: function(key, val) {
        if (lstorage.o) {
            lstorage.o[key+' '+location.hostname] = val;
        }
    },
    g: function(key) {
        if (lstorage.o) {
            return lstorage.o[key+' '+location.hostname] ? lstorage.o[key+' '+location.hostname] : '';
        } else {
            return '';
        }
    },
    r: function(key) {
        if (lstorage.o) {
            lstorage.o.removeItem(key+' '+location.hostname);
        }
    },
    clear: function(pre) {
        if (lstorage.o) {
            for (var a in lstorage.o) {
                if (a.search(pre) != -1) {
                    lstorage.o.removeItem(a);
                }
            }
        }
    }
};

var main = {
    init: function() {

    },
    // functions
    show_search: function() { // открыть форму поиска
        var $search = $('#search');
        if ($search.hasClass('show')) {
            $search.removeClass('show');
        } else {
            $search.addClass('show');
            if ($search.is(':empty')) {
                $search.html(load).load('/search');
            }
        }
    }
};

// системные сообщения
var sys = {
    n: 1,
    add: function(txt, red) {
        red = red || '';
        $('#sys-messages').prepend('<div id="sys-messages'+sys.n+'" onclick="sys.del('+sys.n+')"><div class="mess '+(red != '' ? 'no' : 'ok')+'">'+txt+'</div></div>');
        setTimeout('sys.del('+sys.n+')',15000);
        sys.n++;
    },
    del: function(n) {
        $('#sys-messages'+n).fadeOut();
        setTimeout('$("#sys-messages'+n+'").remove()',5000);
    }
};

// работа с навигационной панелью
var navbar = {
    page_scroll: function() {
        $('html, body').stop().animate({
            scrollTop: ($($(this).attr('href')).offset().top - 54)
        }, 1000, 'easeInOutExpo');
        return false;
    }
};

$(function () {
    new WOW().init();

    $('body').scrollspy({target: '#navSpy', offset: 120}).on('activate.bs.scrollspy');

    $('#topNav').affix({offset: {top: 200}});

    $('a.page-scroll').on('click', navbar.page_scroll);

    $('.navbar-collapse ul li a').on('click', function() {$('.navbar-toggle:visible').click();});

    $('.code').each(function(i, block) {
        hljs.highlightBlock(block);
    });
});